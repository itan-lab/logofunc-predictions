# LoGoFunc predictions

Predictions indicating variants resulting in pathogenic gain- or loss-of-function or neutral variants for canonical missense variants in the human genome. Variant coordinates correspond to the GRCh38/hg38 genome assembly. 

Columns "LoGoFunc_neutral", "LoGoFunc_GOF", "LoGoFunc_LOF" correspond to the predicted probability that a variant is neutral, results in pathogenic gain-of-function, or results in pathogenic loss-of-function respectively. Higher scores indicating higher probability as predicted by the model. 
